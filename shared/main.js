// code that is shared between client and server, i.e. sent to both

// method definitions
Meteor.methods({
  // adding chats
  addChat:function(chat){
    console.log("addChat method running!");
    if (this.userId){// we have a user
      return Chats.insert(chat);
    }
    return;
  }, 
  // updating chats
  updateChat:function(chatid, chat){
    console.log("updateChat method");
    //console.log(chat);
    var realChat = Chats.findOne({_id:chatid});
    if (realChat){
      Chats.update(chatid, chat);
    }
  },
})